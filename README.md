# Shane Rice readme

I'm a curious marketer who loves meeting new people and trying new things. I've worked on remote teams since 2014 and love learning how to replicate the best part of office life with out an office.

This doc is a way for me to document what I see in myself, and hopefully, other people can contribute to help me improve on things I could do better. Please contribute any changes you think would be helpful to my team or me! 😊

### In a nutshell

I like to have fun and connect with people while delivering results as a marketer. I spend half of my waking hours at work, and I want to create an environment where teams thrive. 

To deliver on this purpose I strive to be kind, inclusive, and a focused listener. 

### Important things to know about me
My family is my number one priority. Jen is my partner and we have three kids, so there are times when I need to shift work to pick someone up from school or take them to an appointment.

I have ADHD and was diagnosed as an adult in 2021. This is something I've lived with my whole life, and I only learned it was ADHD as I looked at symptoms on [addittude.com](https://www.additudemag.com/). 

For me, ADHD is not an inability to focus. Instead, I'm able to bring intense focus to things I find interesting while procrastinating on things I feel uncertain about. ADHD also leads me to be a literal and direct communicator because my brain can miss cues for sarcasm, especially in written text. 

I'm actively working to address my ADHD with daily medication and self-reflection. This readme is one of those self-reflections.

My process focus right now is improving my daily workflow with detailed todo lists. This helps me document what I need to do and gives me the ability to look back at my week to see what I've accomplished. Seeing my work in this format helps quiet my self-doubt.

Sharing this makes me feel vulnerable. If you have questions about my ADHD please ask because I'm happy to share what I've learned about my brain 🧠

### Areas of improvement

**Executive function** — I tend to focus on things I find interesting and this can cause me to avoid tasks I find challenging or cause me to spend too much time on a task I find super interesting. I'm balancing this out by keeping a prioritized list of my tasks and making sure I don't let items sit on my to-do list because I don't enjoy them.

**Low-level of shame** — I don't like releasing content, especially writing. My internal resistance is a fear that I won't communicate what I want to say or that other people have said something better. Encourage me to release things before I think they're ready.

**Communicating to my audience** — When I understand a topic, I like to go deep to answer a question. This is great when working through a problem on a team but not ideal for summaries. If I'm going too deep, you can stop me and ask me to summarize.

### Work roles

**Problem solver** — When I encounter a problem, my first response is to try and fix it. This is great when people want help with a problem, but it can be frustrating when someone is venting and doesn't want advice. Please tell me if I'm trying to solve something you only want to talk about.

**Mentor** — I'm always happy to share my career experience or share what I've learned in marketing over the years.

**Marketer** — I've helped grow revenue at SaaS companies for over ten years. During this time, I've done a little bit of everything. This includes SEO, paid campaigns, email, analytics, content, content strategy, automation, field marketing, community marketing, and basic PR.

**Team/Project Lead** — Sometimes, I take the lead on projects that span multiple teams, set goals, and help drive business results.

**Manager** — I work hard to create direction for our team's work and empower everyone on my team to set their own goals to support that direction. We work together to establish KPIs and goals for our team and each individual's output. I work to unblock my team and make sure they have everything they need to be successful.

### How I work

**Disagree and commit** — I have passionate opinions, especially about what makes good marketing work. If I'm not the [directly responsible individual](https://about.gitlab.com/handbook/people-group/directly-responsible-individuals/), I will be sure to voice my disagreement during the decision-making process. Once we've decided, it's my job to commit and support the team's work. Especially when I disagree.

**Be quick, but don't hurry** — We make mistakes when we hurry. I don't like to make mistakes. I also want to get things done. I like to plan my work and deliver it on a predictable schedule. There will always be unplanned work, and my goal is to respond quickly without being in a hurry while delivering my planned work.

**Deep work** — Knowledge work requires space to spend big chunks of time with uninterrupted focus. I prioritize this for myself and work to prioritize it for other people too.

**Skip to the end** — I want to work as close to the end result as possible. Here's an example of what I mean. We need to update or create a landing page. We could spend time laying out all the details in an issue and discussing all the things we need. Instead of this we can work on a first version of the landing page and share that in an issue to improve the result directly. It's faster to start the work, share it, discuss proposed changes, and improve the final result than spending days creating a plan. 

**Automate all the things** — I don't like to repeat tasks. If we can create a process that's reliably managed by an app, that's great. I'd much rather spend extra time upfront to understand the app and save time long-term by taking work off everyone's plate.

### How I communicate

**Async first** — Asynchronous communication is a key for successfully working with remote teams. Email or project management apps are the best place to start collaboration or send me a task. Slack is great for coordinating work on the fly and staying in touch with your team, but it's ephemeral and shouldn't store critical work info. Video is great for check-ins and unblocking a project.

**Video when needed** — Video is a great format for me to quickly share what I'm thinking and working on. I can do this with writing, but it takes me longer because I tend to process my ideas by talking them through. I use [Screenflow](https://www.telestream.net/screenflow/overview.htm) to record my video and desktop for my async first workflow.

**Transparent** — I ask questions and share what I'm working on in public channels. 1:1 messages might not ping as many people, but talking in public channels helps knowledge spread across teams.

**In-depth ** — I strive to be clear when I communicate and make sure everyone understands the details when I present info. You can always stop and let me know that you understand or know something already and we can move to new info faster.

**Document processes** — I've been documenting my thoughts and work in [Reflect](https://reflect.app) with an archive of content in [Craft](https://www.craft.do/) I'm slowly migrating. I'm collecting what I learn to help improve my personal retention of data. I remember lots of things and I save time by remembering to find that article about Groupon deindexing their site from Google in my notes without having to Google it.

**Take notes** — One of the most important things I learned at GitLab is the power of taking notes during a call. It gives everyone on the call a great way to record their discussion and makes it simpler to share across your team or with the world. My preference is work from a shared agenda while encouraging everyone to contribute to the notes on any call. Not only is it unfair to ask one person to take all the notes for a meeting, limiting the perspective of the note taker to one person lowers the quality of notes.

**Todo list** — I keep a work and personal to-do list for every week. If you want to make sure something gets done, ask me to add it to this list. Hopefully, I already have it on there. 😊

## Favorite tools

### Mac OS

#### Direct downloads
[Alfred](https://www.alfredapp.com/)  
[Bartender](https://www.macbartender.com/)  
[CleanShot](https://cleanshot.com/)  
[MenuWhere](https://manytricks.com/menuwhere/)  
[LoopBack](https://rogueamoeba.com/loopback/)  
[Audio Hijack](https://rogueamoeba.com/audiohijack/)  
[SoundSource](https://rogueamoeba.com/soundsource/)  
[Choosy](https://www.choosyosx.com/)  
[Reflect](https://reflect.app/)  
[Little Snitch](https://www.obdev.at/products/littlesnitch/index.html)  
[Mullvad VPN](https://mullvad.net/)  
[Soulver 3](https://soulver.app/)  
[VS Code](https://code.visualstudio.com/)  
[Rocket](https://matthewpalmer.net/rocket/)  
[Screenflow](https://www.telestream.net/screenflow/overview.htm)  

#### App store downloads
[Magnet](https://apps.apple.com/us/app/magnet/id441258766?mt=12)  
[Tot](https://apps.apple.com/us/app/tot/id1491071483?mt=12)  
[Fantastical](https://apps.apple.com/us/app/fantastical-calendar-tasks/id975937182?mt=12)  
[Craft](https://apps.apple.com/us/app/craft-docs-and-notes-editor/id1487937127)  
[CARROTWeather](https://apps.apple.com/us/app/carrot-weather/id993487541?mt=12)  
[Kaliedoscope](https://apps.apple.com/us/app/kaleidoscope-3/id1575557335?mt=12)  
[Time Zone Pro](https://apps.apple.com/us/app/time-zone-pro/id1557558517)  
[Tweetbot](https://apps.apple.com/us/app/tweetbot-3-for-twitter/id1384080005?mt=12)  
